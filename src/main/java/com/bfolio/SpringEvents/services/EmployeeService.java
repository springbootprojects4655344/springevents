package com.bfolio.SpringEvents.services;



import com.bfolio.SpringEvents.dao.EmployeeRepository;
import com.bfolio.SpringEvents.events.AddEmployeeEvent;
import com.bfolio.SpringEvents.exception.ApplicationException;
import com.bfolio.SpringEvents.models.Employee;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService implements ApplicationEventPublisherAware {

    EmployeeRepository repository;
    ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public EmployeeService(EmployeeRepository repository) {
        this.repository = repository;
    }

    public Employee create(Employee employee) throws ApplicationException {
        Employee newEmployee = repository.save(employee);
        if (newEmployee != null) {
            applicationEventPublisher.publishEvent(new AddEmployeeEvent(newEmployee));
            return newEmployee;
        }
        throw new ApplicationException("Employee could not be saved");
    }
}