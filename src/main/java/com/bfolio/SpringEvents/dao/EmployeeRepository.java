package com.bfolio.SpringEvents.dao;

import com.bfolio.SpringEvents.models.Employee;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends ListCrudRepository<Employee, Long> {
}