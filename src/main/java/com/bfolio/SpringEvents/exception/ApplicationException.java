package com.bfolio.SpringEvents.exception;

public class ApplicationException extends Exception {
    public ApplicationException(String msg) {
        super(msg);
    }
}