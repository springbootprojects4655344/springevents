package com.bfolio.SpringEvents.listeners;

import com.bfolio.SpringEvents.events.AddEmployeeEvent;
import com.bfolio.SpringEvents.events.DeleteEmployeeEvent;
import com.bfolio.SpringEvents.services.JavaSmtpGmailSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmployeeEventsListener {

    @Autowired
    private JavaSmtpGmailSenderService senderService;
    @EventListener
    void handleAddEmployeeEvent(AddEmployeeEvent event) {
        senderService.sendEmail("comlan-beh-ardieu.alotin@ratp.fr","Test","This is email body");
        log.info(event.toString());
    }

    @EventListener
    void handleDeleteEmployeeEvent(DeleteEmployeeEvent event) {
        log.info(event.toString());
    }
}