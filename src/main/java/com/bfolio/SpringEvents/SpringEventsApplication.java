package com.bfolio.SpringEvents;


import com.bfolio.SpringEvents.models.Employee;
import com.bfolio.SpringEvents.services.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SpringEventsApplication implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(SpringEventsApplication.class);
	}

	@Autowired
	EmployeeService employeeService;

	@Override
	public void run(String... args) throws Exception {

		employeeService.create(new Employee("Beh ALOTIN"));
		log.info("New Employee Created...");
	}
}