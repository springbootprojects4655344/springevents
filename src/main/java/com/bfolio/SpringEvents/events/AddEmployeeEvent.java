package com.bfolio.SpringEvents.events;

import com.bfolio.SpringEvents.models.Employee;
import com.bfolio.SpringEvents.services.JavaSmtpGmailSenderService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public class AddEmployeeEvent {

    @Getter
    private Employee employee;

    @Autowired
    private JavaSmtpGmailSenderService senderService;

    public AddEmployeeEvent(Employee employee) {
        this.employee = employee;
    }

    public void sendMail(){
        senderService.sendEmail("comlan-beh-ardieu.alotin@ratp.fr","Test","This is email body");
    }

    @Override
    public String toString() {
        return "ApplicationEvent: New Employee Saved :: " + this.employee;
    }
}