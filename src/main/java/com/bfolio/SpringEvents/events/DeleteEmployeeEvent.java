package com.bfolio.SpringEvents.events;

import com.bfolio.SpringEvents.models.Employee;
import org.springframework.context.ApplicationEvent;

public class DeleteEmployeeEvent extends ApplicationEvent {

    public DeleteEmployeeEvent(Employee employee) {
        super(employee);
    }

    @Override
    public String toString() {
        return "ApplicationEvent: Employee Deleted :: " + this.getSource();
    }
}